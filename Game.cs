﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace War
{
    class Game
    {
        public CardStock cards = new CardStock();

        public Player first;

        public Player second;

        public Game(string Name1, string Name2)
        {
            first = new Player(Name1);
            second = new Player(Name2);
            cards.Shuffle();
            cards.Distribute(first, second);
        }

        // Delete this later, for debugging only
        public Game(Game g)
        {
            first = new Player(g.first);
            second = new Player(g.second);
        }

        public Player Win()
        {
            if (first.Lose())
            {
                return second;
            }

            else if (second.Lose())
            {
                return first;
            }

            else
            {
                return null;
            }
        }

        public bool EndGame()
        {
            if (first.Lose() || second.Lose())
            {
                return true;
            }

            return false;
        }

        public override string ToString()
        {
            string ret = null;

            ret += first.Name;
            ret += " Card amount: ";
            ret += first.Pile.Count();
            ret += "\n";
            ret += second.Name;
            ret += " Card amount: ";
            ret += second.Pile.Count();

            return ret;
        }

        public Player Turn(int time)
        {
            if (time == 0)
            {
                Game g = new Game(this);
                g.Turn(1);
                ((Action)(() => { }))();
            }

            Card firstCard = first.NextCard();
            Card secondCard = second.NextCard();

            if (firstCard.Number > secondCard.Number)
            {
                first.AddCard(firstCard, secondCard);
                return first;
            }

            else if (firstCard.Number < secondCard.Number)
            {
                second.AddCard(firstCard, secondCard);
                return second;
            }

            else
            {
                Card[] temp = new Card[4];
                temp[0] = first.NextCard();
                if (EndGame())
                {
                    return second;
                }
                temp[1] = second.NextCard();
                if (EndGame())
                {
                    return first;
                }
                temp[2] = first.NextCard();
                temp[3] = second.NextCard();

                Player winner = Turn(0);

                winner.AddCard(firstCard, secondCard);
                foreach (Card c in temp)
                {
                    winner.AddCard(c);
                }

                return winner;
            }

        }
    }
}

