﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace War
{
    class Player
    {
        public string Name;

        public Player(string p_Name)
        {
            Name = p_Name;
        }

        public Player(Player first)
        {
            Name = string.Copy(first.Name);
            Pile = new Queue<Card>(first.Pile.ToArray());
        }

        public Queue<Card> Pile = new Queue<Card>();

        public Queue<Card> AddCard(params Card[] cards)
        {
            foreach (Card c in cards)
            {
                Pile.Enqueue(c);
            }

            return Pile;
        }

        public override string ToString()
        {
            string ret = null;
            ret += Name;
            ret += " Number of cards: ";
            ret += Pile.Count().ToString();
            ret += " Cards: ";
            foreach (Card c in Pile)
            {
                ret += c.ToString();
                ret += " ";
            }

            return ret;
        }

        public bool Lose()
        {
            return Pile.Count() == 0;
        }

        public Card NextCard()
        {
            if (Pile.Count == 0)
            {
                return null;
            }

            return Pile.Dequeue();
        }
    }
}
