﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace War
{
    class Card : IComparable
    {

        public Card(E_Color p_color, int p_number)
        {
            color = p_color;
            number = p_number;
        }

        public override string ToString()
        {
            string ret;
            if (color == E_Color.black)
            {
                ret = "black";
            }

            else
            {
                ret = "red";
            }

            ret += CardName;
            return ret;
        }

        public int CompareTo(object obj)
        {
            return CardName.CompareTo(((Card)obj).CardName);
        }

        public enum E_Color { red, black };


        private E_Color color;

        public E_Color Color
        {
            get
            {
                return color;
            }
            set
            {
                color = value;
            }
        }


        private int number;

        public int Number
        {
            get
            {
                return number;
            }
            set
            {
                if (value >= 2 && value <= 14)
                {
                    number = value;
                }
            }
        }

        public string CardName
        {
            get
            {
                switch (number)
                {
                    case 11:
                        return "Jack";

                    case 12:
                        return "Queen";

                    case 13:
                        return "King";

                    case 14:
                        return "Ace";

                    default:
                        return number.ToString();

                }
            }
        }

    }
}
