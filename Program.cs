﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace War
{
    class Program
    {
        static void Main(string[] args)
        {
            //string first, second;
            //Console.WriteLine("Please enter both players' names:");
            //first = Console.ReadLine();
            //second = Console.ReadLine();
            //Game game = new Game(first, second);
            Game game = new Game("Gabi", "Meir");
            Console.WriteLine(game.ToString());
            do
            {
                game.Turn(0);
            } while (game.EndGame() != true);

            Console.WriteLine(game.Win());
        }
    }
}
