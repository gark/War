﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace War
{
    class CardStock : IEnumerable
    {
        public List<Card> Cards = new List<Card>();

        public CardStock()
        {
            for (int i = 2; i <= 14; ++i)
            {
                Card c = new Card(Card.E_Color.black, i);
                Cards.Add(c);
            }

            for (int i = 2; i <= 14; ++i)
            {
                Card c = new Card(Card.E_Color.red, i);
                Cards.Add(c);
            }
        }

        public void Shuffle()
        {
            Random r = new Random();

            for (int i = 0; i < 100; ++i)
            {
                int a = r.Next(26);
                int b = r.Next(26);
                Card temp = Cards[a];
                Cards[a] = Cards[b];
                Cards[b] = temp;
            }
        }

        public override string ToString()
        {
            string ret = null;
            foreach (Card c in Cards)
            {
                ret += c.ToString();
            }

            return ret;
        }

        public void Distribute(params Player[] players)
        {
            while (Cards.Count() != 0)
            {
                foreach (Player p in players)
                {
                    p.AddCard(Cards[0]);
                    Cards.RemoveAt(0);
                }
            }
        }

        public Card this[string s]
        {
            get
            {
                foreach (Card c in Cards)
                {
                    if (c.CardName.CompareTo(s) != 0)
                    {
                        return c;
                    }
                }

                return null;
            }

            set
            {

            }
        }

        public void Sort()
        {
            List<Card> ret = new List<Card>();
            for (int i = 0; i < Cards.Count(); ++i)
            {
                Card smallest = Cards[i];
                foreach (Card c in Cards)
                {
                    if (c.Number < smallest.Number)
                    {
                        smallest = c;
                    }
                }

                Cards[i] = smallest;
            }
        }

        public IEnumerator GetEnumerator()
        {
            return Cards.GetEnumerator();
        }

        public CardStock AddCard(Card card)
        {
            Cards.Add(card);
            return this;
        }

        public void RemoveCard()
        {
            Cards.RemoveAt(Cards.Count() - 1);
        }
    }
}
